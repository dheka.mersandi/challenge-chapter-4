class App {
  constructor() {
    this.clearButton = document.getElementById("clear-btn");
    this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");
  }

  async init() {
    await this.load();
  }

  run = () => {
    Car.list.forEach((car) => {
      const node = document.createElement("div");
      node.innerHTML = car.render();
      this.carContainerElement.appendChild(node);
    });
  };

  async load() {
    this.clear();
    let cars = [];
    cars = await Binar.listCars();

    // DOM for selecting input from user
    let inputTime = document.getElementById("inputTime").value;
    let inputDate = document.getElementById("inputDate").value;
    let inputDateTime = inputDate + "T" + inputTime + "Z";
    let inputCapacity = document.getElementById("jumlahPenumpang").value;

    // stop or return false if time or date not initialized
    if (inputTime == "" || inputDate == "") {
      alert("Input date or time first");
      return false;
    }
    // filtering array
    let filteredCars = cars.filter((car) => {
      if (inputCapacity === "") {
        return car.available === true && Date.parse(car.availableAt) > Date.parse(inputDateTime);
      } else {
        return (
          car.available === true &&
          Date.parse(car.availableAt) > Date.parse(inputDateTime) &&
          car.capacity >= inputCapacity
        );
      }
    });
    Car.init(filteredCars);
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}
